import { UiButton } from './index'

export default {
  title: 'Components/UIButton',
  component: UiButton,
  argTypes: {
    className: ''
  }
}

const Template = (args) => <UiButton {...args}>sample component</UiButton>

export const Primary = Template.bind({})
Primary.args = {
  primary: true,
  label: 'UiButton'
}
