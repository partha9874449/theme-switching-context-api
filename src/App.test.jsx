import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import App from './App'

jest.mock('react-fetch-hook', () =>
  jest
    .fn()
    .mockReturnValueOnce({ isLoading: true, data: null })
    .mockReturnValue({
      isLoading: false,
      data: {
        heading: {
          value: 'React webpack boilerplate'
        }
      }
    })
)

describe('App.jsx', () => {
  test('should render loading while fetching content api', () => {
    const { getByText } = render(<App />)
    expect(getByText('loading...')).toBeInTheDocument()
  })
  test('should render heading on success of content api', () => {
    const { getByText } = render(<App />)
    expect(getByText('React webpack boilerplate')).toBeInTheDocument()
  })
})
